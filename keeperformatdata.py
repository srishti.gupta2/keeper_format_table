from pymongo import MongoClient
import pandas as pd

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://dl-user-1:A2mClcihXGlNQC7M@datalake0-3comg.a.query.mongodb.net/Database0?ssl=true&authSource=admin')
result = client['Database0']['Order_Commission_Invoice_3'].aggregate([
    {
        '$match': {
            'payment_status.code': 'success'
        }
    }, {
        '$project': {
            '_id': 0, 
            'Order Date': '$created_at', 
            'Order ID': '$order_id', 
            'Brand Name': '$brand_info.name', 
            'Product SKU': '$items.catalog_info.variant.sku', 
            'Product Name': '$items.catalog_info.name', 
            'Quantity': '$items.quantity', 
            'Shipping Display Name': '$invoice_info.shipping_address.display_name', 
            'Shipping Address': '$invoice_info.shipping_address.plain_address', 
            'Shipping Number': '$invoice_info.shipping_address.phone_number.number', 
            'Payment Mode': {
                '$cond': {
                    'if': {
                        '$eq': [
                            '$payment_mode', 'cod'
                        ]
                    }, 
                    'then': 'cod', 
                    'else': 'prepaid'
                }
            }, 
            'Courier Name': '$courier_name', 
            'Tracking ID': '$tracking_id', 
            'Retail Price': '$items.retail_price.value', 
            'Grand Total': '$grand_total.value'
        }
    }
])
df = pd.DataFrame(result)
df.to_csv("keeperformat.csv")