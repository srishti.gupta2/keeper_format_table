import datetime as dt
import copy
import os
import dash
import datetime as dt
from dash import dcc
import dash_bootstrap_components as dbc
from dash import html
from dash import dash_table
from dash.dependencies import Input, Output
from numpy import isin, place
import pandas as pd
import pymongo
import flask


def init_dash_app(requests_pathname_prefix, server):
    app = dash.Dash(__name__, requests_pathname_prefix=requests_pathname_prefix, server=server)
    app.scripts.config.serve_locally = False
    dcc._js_dist[0]['external_url'] = 'https://cdn.plot.ly/plotly-basic-latest.min.js'
    df=pd.read_csv("./keeperformat.csv",low_memory=False)

# app=dash.Dash('/commissioncredit')
# app = dash(__name__)
    app.layout = html.Div([
        dcc.Markdown('# Orders & Creators', style={'textAlign':'center', 'font-family': 'arial', 'fontSize': '10px'}),
        html.Div([
        dbc.Row([
        #     dbc.Col([
        #         mop_drop := dcc.Dropdown([x for x in sorted(df['Mode of Payment'].unique())], multi= True, placeholder='Select the Mode of Payment(s)..', style={'height': '48px', 'width': '300px'}, id = 'mop_drop')
        #     ],style={'display': 'flex',
        # 'align-items': 'center'}),
        dbc.Col([
                dcc.DatePickerRange(id='my-date-picker-range',min_date_allowed=dt.datetime(2018, 1, 1),max_date_allowed=dt.datetime(2022, 12, 12),initial_visible_month=dt.datetime(2022, 1, 1), with_portal=True, clearable = True, updatemode='singledate',style={'height':'30px','border-radius':'5px','font-size':'16px'}),
            # html.Br(),
        ]),
        #     dbc.Col([
        #         brand_drop := dcc.Dropdown([x for x in sorted(df['Brand Name'].unique())], multi=True, placeholder='Select the Brand(s)..', style={'height': '48px', 'width': '300px'}, id = 'brand_drop')
        #     ],style={'display': 'flex',
        # 'align-items': 'center'})

        ], justify="center",style = {'display': 'flex','gap':"20px"}),
        ],
        style = {'margin': '16px'}

        ),

        dash_table.DataTable(
            id='datatable-interactivity',
            columns=[
                {"name": i, "id": i, "deletable": True, "selectable": True} for i in df.columns
            ],
            data=df.to_dict('records'),
            export_format="csv",
            export_headers="display",
            style_cell={'textAlign': 'left', 'padding': '5px', 'minWidth': 170, 'width': 250, 'maxWidth': 250, 'font-family': 'arial', 'fontSize': '12px', 'border': '1px solid rgb(211, 211, 211)' },
            style_as_list_view=True,
            style_header={
                'fontWeight': 'bold'
                },
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)',
                    'if': {'column_id': 'Order ID'},
                }
            ],
            virtualization = True,
            fixed_rows = {'headers' : True},
            style_table={'height': "100%", 'border-radius': 10, 'box-shadow': 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px', 'marginTop': '10px', 'border': '1px solid rgb(211, 211, 211)'},
            editable = False,
            filter_action = "native",
            sort_action = 'native',
            sort_mode = 'multi',
            column_selectable = False,
            row_deletable = False,
            selected_columns=[],
            selected_rows=[],
            page_action="native",

        ),

        html.Div(id='datatable-interactivity-container', style = {'margin': '10px'}),
    ])

    @app.callback(
        Output('datatable-interactivity', 'data'),
        [Input('my-date-picker-range', 'start_date'),
        Input('my-date-picker-range', 'end_date')
        # Input('mop_drop', 'value'),
        # Input('brand_drop', 'value')
        ]
    )
    def update_data(start_date, end_date):

        df_temp = copy.deepcopy(df)
        ctx = dash.callback_context
        changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]

        ctx_inputs = ctx.inputs
        dpsd = ctx_inputs['my-date-picker-range.start_date']
        dped = ctx_inputs['my-date-picker-range.end_date']
        # mpd = ctx_inputs['mop_drop.value']
        # bdp = ctx_inputs['brand_drop.value']

        if dpsd is not None and dped is not None:
            print(dpsd, dped)
            sd = dt.datetime.strptime(dpsd, "%Y-%m-%d").date()
            ed = dt.datetime.strptime(dped, "%Y-%m-%d").date()
            df_temp = df_temp.loc[(df["Order Date"] >= sd) & (df["Order Date"] <= ed)]

        # if mpd:
        #     print(mpd)
        #     df_temp = df_temp[df_temp['Mode of Payment'].isin(mpd)]

        # if bdp:
        #     print(bdp)
        #     df_temp = df_temp[df_temp['Brand Name'].isin(bdp)]

        return df_temp.to_dict('records')

    return app
# if __name__ == "__main__":
#     app.run_server(debug=False, dev_tools_ui=False)


def keeper(requests_pathname_prefix: str = None) -> dash.Dash:
    server = flask.Flask(__name__)
    app = init_dash_app(requests_pathname_prefix, server)
    return app